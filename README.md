# EnigmaKit

**EnigmaKit** is a Swift package designed to replicate the Enigma machine used by Germany in WWII.  You can read more about its operation [here](https://en.wikipedia.org/wiki/Enigma_machine).

# Configuration of the Enigma Machine
The Enigma machine consisted of several parts that must all be set exactly the same between two Enigma machines for proper communication.  When using the machine, the operator would set the following parts to encrypt a message:  

- 3 rotors, including:
  - The order of the rotors
  - The ring settings of the rotors
  - The starting positions of the rotors
- A reflector
- Plugboard connections

## Rotors 

The Enigma machine came with a set of rotors that were used for mapping letters to different letters during encryption and decryption.  The operator chooses 3 of these rotors and places them in the machine in any order. 

**EnigmaKit** implements rotors I, II, and III that were introduced with the Enigma I in 1930 as well as rotors IV and V from 1938 and VI, VII, and VII from 1939.  These rotors have the following 'internal wiring' for the standard latin alphabet:

- Rotor I    - EKMFLGDQVZNTOWYHXUSPAIBRCJ
- Rotor II   - AJDKSIRUXBLHWTMCQGZNPYFVOE
- Rotor III  - BDFHJLCPRTXVZNYEIWGAKMUSQO
- Rotor IV   - ESOVPZJAYQUIRHXLNFTGKDCMWB
- Rotor V    - VZBRGITYUPSDNHLXAWMJQOFECK
- Rotor VI   - JPGVOUMFYQBENHZRDKASXLICTW
- Rotor VII  - NZJHGRCXMYSWBOUFAIVLPEKQDT
- Rotor VIII - FKQHTLXOCBJSPDZRAMEWNIUYGV

These rotors have notches on them that interface with the machine so one rotor can move the next one in sequence.  When a rotor steps past the following letters, the next rotor in sequence turns.  Note that rotors VI, VII, and VIII have 2 notches so the next rotor will turn twice in 1 revolution.

- Rotor I    - Q
- Rotor II   - E
- Rotor III  - V
- Rotor IV   - J
- Rotor V    - Z
- Rotor VI, VII, VIII  - Z + M

The letters on a rotor were printed on a ring that could also be adjusted, meaning that the same rotor could have 26 possible letter mappings.  This is referred to as the ring setting.

When placing the rotors in the Enigma Machine, they could also be placed in one of 26 different starting rotations.  This controlled what letter the mappings started on and is reffered to as the rotor position.

Finally, when placing the rotors in the Enigma machine, the operator could place them in any order, controlling the order in which the mapping takes place.

## Reflector
The reflector is required to route the mapping signals back into the rotors and is what allows the machine to encrypt and decrypt messages without changing modes.  It also has the side effect of making it so no letter can map to itself.

**EnigmaKit** has a choice of 3 reflectors - A, B, and C -  with the following 'wiring':

- A - ZYXWVUTSRQPONMLKJIHGFEDCBA
- B - YRUHQSLDPXNGOKMIEBFZCWVJAT
- C - FVPJIAOYEDRZXWGCTKUQSBNMHL

## Plugboard
The plugboard allowed variable wiring that could be reconfigured by the operator,  adding over 150 trillion possible settings to the machine.  This consists of a pair of letters that will be swapped during encryption and decription.  **EnigmaKit** contains a `Plug` struct that can be used keep track of pairs of letters, or it can also be inialized with a string of letter pairs, i.e. ("AB CD EF").
# Usage
 
Using **EnigmaKit** first requires that an Enigma machine be configured.  

Conveniently, the `Enigma` struct has an initializer that allows you to set all of these values at once.

    var enigma = try Enigma(rotors: [.I, .II, .III], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [Plug(("A", "Z"))])

You can also input a string of letter pairs to the `plugboardConnections` argument.

    var enigma = try Enigma(rotors: [.I, .II, .III], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: "AZ BY")

Once you have an `Enigma` struct, you can call the `encrypt(_: String) -> String` or `encrypt(_: Character) -> Character` method to start encrypting.

    let encryptedMessage = enigma.encrypt("Hello World")
    let encryptedCharacter = enigma.encrypt("H")

Since encrypting using an Enigma machine depends on the state of the machine, both of the encrypt methods are `mutating`, as encrypting changes the internal state of the Enigma machine.
