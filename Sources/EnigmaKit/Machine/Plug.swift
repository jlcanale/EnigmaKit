//
//  Plug.swift
//  
//
//  Created by Joe Canale on 4/25/22.
//

import Foundation

public struct Plug: Equatable {
    let firstPlug: Character
    let secondPlug: Character
    
    init(firstPlug: Character, secondPlug: Character) {
        self.firstPlug = firstPlug
        self.secondPlug = secondPlug
    }
    
    init(_ pair: (firstCharacter: Character, secondCharacter: Character)) {
        self.init(firstPlug: pair.firstCharacter, secondPlug: pair.secondCharacter)
    }
}
