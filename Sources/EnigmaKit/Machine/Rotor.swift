//
//  Rotor.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/19/22.
//

import Foundation

struct Rotor {
    let name: String
    let forwardWiring: [Int]
    let backwardWiring: [Int]
    
    var rotorPosition: Int
    let notchPosition: Int
    let ringSetting: Int
    
    let notchClosure: ((Int) -> Bool)?
    
    var isAtNotch: Bool {
        if let notchClosure = notchClosure {
            return notchClosure(rotorPosition)
        } else {
            return notchPosition == rotorPosition
        }
    }
    
    init(rotorType: RotorType, rotorPosition: Int, ringSetting: Int) {
        self.name = rotorType.rawValue
        self.forwardWiring = rotorType.encoding.letterIndexes()
        self.backwardWiring = forwardWiring.inverse()
        self.rotorPosition = rotorPosition
        self.notchPosition = rotorType.notchPosition
        self.ringSetting = ringSetting
        self.notchClosure = rotorType.notchClosure
    }
    
    static func encipher(k: Int, position: Int, ring: Int, mapping: [Int]) -> Int {
        let shift = position - ring
        return (mapping[(k + shift + 26) % 26] - shift + 26) % 26
    }
    
    func forward(c: Int) -> Int {
        return Self.encipher(k: c, position: rotorPosition, ring: ringSetting, mapping: forwardWiring)
    }
    
    func backward(c: Int) -> Int {
        return Self.encipher(k: c, position: rotorPosition, ring: ringSetting, mapping: backwardWiring)
    }
    
    mutating func turnover() {
        rotorPosition = (rotorPosition + 1) % 26
    }
}
