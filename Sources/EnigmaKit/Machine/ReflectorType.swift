//
//  ReflectorType.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/22/22.
//

import Foundation

public enum ReflectorType: String {
    case A
    case B
    case C
    
    public var encoding: String {
        switch self {
            case .A:
                return "ZYXWVUTSRQPONMLKJIHGFEDCBA"
            case .B:
                return "YRUHQSLDPXNGOKMIEBFZCWVJAT"
            case .C:
                return "FVPJIAOYEDRZXWGCTKUQSBNMHL"
        }
    }
}
