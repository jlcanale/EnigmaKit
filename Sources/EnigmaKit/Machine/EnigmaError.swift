//
//  EnigmaError.swift
//  
//
//  Created by Joe Canale on 4/25/22.
//

import Foundation

enum EnigmaError: Error, LocalizedError {
    case invalidPairLength(String)
    case duplicatePlug(String)
    case invalidCharacter(Character)
    case invalidRotorCount
    case invalidRotorPositionCount
    case invalidRingSettingsCount
    
    var errorDescription: String? {
        switch self {
            case .invalidPairLength(let string):
                return "Error decoding plug string: \(string). String must contain pairs of letters."
            case .duplicatePlug(let string):
                return "Error decoding plug string: \(string). A letter can only be mapped to one other letter."
            case .invalidCharacter(let character):
                return "Invalid Character Error: \(character).  Plugs are only allowed to contain the characters A-Z"
            case .invalidRotorCount:
                return "You must select 3 rotors."
            case .invalidRotorPositionCount:
                return "You must select 3 starting rotor positions."
            case .invalidRingSettingsCount:
                return "You must select 3 starting ring settings."
        }
        
        
    }
}
