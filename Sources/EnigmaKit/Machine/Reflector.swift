//
//  Reflector.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/19/22.
//

import Foundation

struct Reflector {
    let forwardWiring: [Int]
    
    init(reflectorType: ReflectorType) {
        self.forwardWiring = reflectorType.encoding.letterIndexes()
    }
    
    func forward(c: Int) -> Int {
        return forwardWiring[c]
    }
}
