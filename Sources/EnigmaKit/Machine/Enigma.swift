//
//  Enigma.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/19/22.
//

import Foundation

public struct Enigma {
    
    private var leftRotor: Rotor
    private var middleRotor: Rotor
    private var rightRotor: Rotor
    private let reflector: Reflector
    private let plugboard: Plugboard
    
    public init(rotors: [RotorType], reflector: ReflectorType, rotorPositions: [Int], ringSettings: [Int], plugboardConnections: String) throws {
        try self.init(rotors: rotors, reflector: reflector, rotorPositions: rotorPositions, ringSettings: ringSettings, plugboardConnections: Plugboard.decode(input: plugboardConnections))
    }
    
    public init(rotors: [RotorType], reflector: ReflectorType, rotorPositions: [Int], ringSettings: [Int], plugboardConnections: [Plug]) throws {
        guard rotors.count == 3 else { throw EnigmaError.invalidRotorCount }
        guard rotorPositions.count == 3 else { throw EnigmaError.invalidRotorPositionCount }
        guard ringSettings.count == 3 else { throw EnigmaError.invalidRingSettingsCount }


        self.leftRotor = Rotor(rotorType: rotors[0], rotorPosition: rotorPositions[0], ringSetting: ringSettings[0])
        self.middleRotor = Rotor(rotorType: rotors[1], rotorPosition: rotorPositions[1], ringSetting: ringSettings[1])
        self.rightRotor = Rotor(rotorType: rotors[2], rotorPosition: rotorPositions[2], ringSetting: ringSettings[2])
        self.reflector = Reflector(reflectorType: reflector)
        self.plugboard = try Plugboard(plugs: plugboardConnections)
    }

    private mutating func rotate() {
        //if middle rotor notch - double-stepping
        if (middleRotor.isAtNotch) {
            middleRotor.turnover()
            leftRotor.turnover()
        } else if (rightRotor.isAtNotch) {
            //If the left-rotor notch
            middleRotor.turnover()
        }
        
        //Increment the right-most rotor
        rightRotor.turnover()
    }
    
    private mutating func encrypt(_ c: Int) -> Int {
        rotate()
        var c = c

        //Plugboard In
        c = plugboard.forward(c: c)
        
        //Right to Left
        let c1 = rightRotor.forward(c: c)
        let c2 = middleRotor.forward(c: c1)
        let c3 = leftRotor.forward(c: c2)
        
        //Reflector
        let c4 = reflector.forward(c: c3)

        //Left to Right
        let c5 = leftRotor.backward(c: c4)
        let c6 = middleRotor.backward(c: c5)
        let c7 = rightRotor.backward(c: c6)

        //Plugboard Out
        let c8 = plugboard.forward(c: c7)
        
        return c8
    }
    
    public mutating func encrypt(_ c: Character) -> Character {
        return Character(UnicodeScalar(encrypt(Int((c.asciiValue ?? 0) - 65)) + 65) ?? UnicodeScalar(0))
    }
    
    public mutating func encrypt(_ message: String) -> String {
        var output = ""
        let message = message.uppercased().replacingOccurrences(of: " ", with: "")
        
        for character in message {
            output.append(encrypt(character))
        }
        
        return output
    }
}
