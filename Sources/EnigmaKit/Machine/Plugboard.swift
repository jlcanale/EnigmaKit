//
//  Plugboard.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/19/22.
//

import Foundation

internal struct Plugboard {
    private let wiring: [Int] = []
    private let plugs: [Plug]
    
    public init(plugs: [Plug]) throws {
        var plugArray = Self.identityPlugboard()
        
        for plug in plugs {
            plugArray.removeAll { $0.firstPlug.uppercased() == plug.firstPlug.uppercased() || $0.firstPlug.uppercased() == plug.secondPlug.uppercased() }
            plugArray.append(Plug(firstPlug: plug.firstPlug.uppercased().firstCharacter, secondPlug: plug.secondPlug.uppercased().lastCharacter))
        }
        self.plugs = plugArray
    }
    
    internal static func decode(input: String) throws -> [Plug]  {
        guard !input.isEmpty else { return [] }
        var plugs = [Plug]()
        let letterPairs = input.split(separator: " ").map { $0.uppercased() }
        
        for letterPair in letterPairs {
            guard letterPair.count == 2 else { throw EnigmaError.invalidPairLength(letterPair) }
            let firstLetter = letterPair.firstCharacter
            let secondLetter = letterPair.lastCharacter
            
            guard firstLetter.isLetter else { throw EnigmaError.invalidCharacter(firstLetter) }
            guard secondLetter.isLetter else { throw EnigmaError.invalidCharacter(secondLetter) }
            guard plugs.first(where: { $0.firstPlug == firstLetter || $0.secondPlug == firstLetter }) == nil else { throw EnigmaError.duplicatePlug(String(firstLetter)) }
            guard plugs.first(where: { $0.firstPlug == secondLetter || $0.secondPlug == secondLetter }) == nil else { throw EnigmaError.duplicatePlug(String(secondLetter)) }
            
            plugs.append(Plug(firstPlug: firstLetter, secondPlug: secondLetter))
        }
        
        return plugs
    }
    
    internal func forward(c: Int) -> Int {
        let output = plugs.first { $0.firstPlug.letterIndex == c }?.secondPlug.letterIndex ?? plugs.first { $0.secondPlug.letterIndex == c }?.firstPlug.letterIndex ?? 0
        return output
    }
    
    private static func identityPlugboard() -> [Plug] {
        return (65...90).map {Plug(firstPlug: (Character(UnicodeScalar($0)!)), secondPlug: Character(UnicodeScalar($0)!)) }
    }
}
