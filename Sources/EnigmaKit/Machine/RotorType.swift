//
//  RotorType.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/22/22.
//

import Foundation

public enum RotorType: String {
    case I
    case II
    case III
    case IV
    case V
    case VI
    case VII
    case VIII
    case identity
    
    public var encoding: String {
        switch self {
            case .I:
                return "EKMFLGDQVZNTOWYHXUSPAIBRCJ"
            case .II:
                return "AJDKSIRUXBLHWTMCQGZNPYFVOE"
            case .III:
                return "BDFHJLCPRTXVZNYEIWGAKMUSQO"
            case .IV:
                return "ESOVPZJAYQUIRHXLNFTGKDCMWB"
            case .V:
                return "VZBRGITYUPSDNHLXAWMJQOFECK"
            case .VI:
                return "JPGVOUMFYQBENHZRDKASXLICTW"
            case .VII:
                return "NZJHGRCXMYSWBOUFAIVLPEKQDT"
            case .VIII:
                return "FKQHTLXOCBJSPDZRAMEWNIUYGV"
            case .identity:
                return "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        }
    }
    
    public var notchPosition: Int {
        switch self {
            case .I:
                return 16
            case .II:
                return 4
            case .III:
                return 21
            case .IV:
                return 9
            case .V:
                return 25
            default:
                return 0
        }
    }
    
    internal var notchClosure: ((Int) -> Bool)? {
        switch self {
            case .VI, .VII, .VIII:
                return { $0 == 12 || $0 == 25 }
            default:
                return nil
        }
    }
}
