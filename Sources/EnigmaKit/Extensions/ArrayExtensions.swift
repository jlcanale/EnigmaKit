//
//  ArrayExtensions.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/22/22.
//

import Foundation

extension Array where Element == Int {
    func inverse() -> [Int] {
        guard !self.isEmpty else { return [] }
        var inverse = [Element](repeating: 0, count: self.count)
        for i in 0..<self.count {
            let forward = self[i]
            inverse[forward] = i
        }
        return inverse
    }
}


extension Array {
    mutating func fill(to capacity: Int, with value: Element) {
        guard self.count < capacity else { return }
        
        for _ in self.count..<capacity {
            self.append(value)
        }
    }
}
