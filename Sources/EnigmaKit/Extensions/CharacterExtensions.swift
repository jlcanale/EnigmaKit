//
//  CharacterExtensions.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/22/22.
//

import Foundation

extension Character {
    var letterIndex: Int {
        return Int((self.asciiValue ?? 65) - 65)
    }
}
