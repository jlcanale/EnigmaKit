//
//  StringExtensions.swift
//  Swiftnigma
//
//  Created by Joe Canale on 4/19/22.
//

import Foundation

extension String {
    var firstCharacter: Character {
        Character(String(self[startIndex...startIndex]))
    }
    
    var lastCharacter: Character {
        Character(String(self[index(before: endIndex)..<endIndex]))
    }
    
    func letterIndexes() -> [Int] {
        return self.map { Int(($0.asciiValue ?? 65) - 65) }
    }
}
