//
//  RotorTests.swift
//  
//
//  Created by Joe Canale on 4/25/22.
//

import XCTest
import EnigmaKit

class RotorTests: XCTestCase {

    func testRotorI() throws {
        var enigma = try! Enigma(rotors: [.I, .I, .I], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "yaqnb afsfn rnohg mlpug uorap djlzc cizdy nhkmt zruwm cxxgp ihsen dqfdr fucwp gskqf fhhbt qinzf ncsec ixkpz ewawy oxzwz xeifj xauqn bqxvn zeoua tdkzb rlypl epnmn byhox zzgzr keirt psttn suirt njzdv fjhjg clrif ccqbw puwww zpimy gywrg t"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testRotorII() throws {
        var enigma = try! Enigma(rotors: [.II, .II, .II], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "xbysb bynyc olyuq eiqyd qobiw umnar lpwtj rkkym zpjgo krepu xtiem lzada ubhly bewyw tfqsu djhov xwron tginh qipor doajp pturt etbwy vtapo mwpwq xwnls qsenj ogyfy fkuwx kjiko hkhft zoqze cgiwq xdeha pxenl dmztp mtxps njqdr dixxw mkbcp c"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testRotorIII() throws {
        var enigma = try! Enigma(rotors: [.III, .III, .III], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "qtgbu gopdh pvnxm xzwzc lyzkt cdizq ttlvj syoxb awcxo mxgvw nenja fhjpv liqwz rmtmv inqyg fwubx uptgo hridi irmzo gmydp kyenb llvju hqtod wtnst gzqeb dpuod fpbdn hbpmx uyvpr hlnrc amxkx llauz kaqaj utnje ufqph vhean inutq oktjb zchku t"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testRotorIV() throws {
        var enigma = try! Enigma(rotors: [.IV, .IV, .IV], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "wcvbs vzmzl iffoa ddcxt bvvbv ouyfx vmlze nrorh wozhb mlits xevty ttrwg aunhs csgku gmgcd hfrwz rdmya arflb nlnpa bgmwl dgwaz vpuio bpnpp rwtnw vptrb mfrso mdnrz wiczr qnpqd hnifr uhiox yociw xbgjn zbtub asjai ekdtf ovdeu nmgyt pwvxh u"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testRotorV() throws {
        var enigma = try! Enigma(rotors: [.V, .V, .V], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "nkyrz nxkku qjimt ikhxw lqpdq rauhs ffyix ptnnp qvrxl twhsn sxvgo nesfr mgsxk ojrnp nydsg rdhee qkjes abiuo doeit fetcm ammqc zxlgn bpkaw xkhmm kpqgy mbrkp jmfdv ujlci qefoh eevjj cyytx xdjub cpcsj mniet mwpxi fshnw iqinz ysgac qieze u"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testRotorVI() throws {
        var enigma = try! Enigma(rotors: [.VI, .VI, .VI], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "xwrlz yyatf hsccj jsavb uddfj xnqtv ykxqj npcbp oeeri mstjd qisxb tionj ysktz izjfu dhvpc jpyuc hjiyk pmqgy bgsnq dcqem dbaqp nwstf gdxkn aboip usykj tiqjj nsdmh zjava kmtxw irjvz wkrfy npdtl lpuiv ctraa naaay zhpza okcjr oxsth henjb c"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testRotorVII() throws {
        var enigma = try! Enigma(rotors: [.VII, .VII, .VII], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "wrclg sdbqj sylyq guyug rtofc qklkw ipchf mokwf xtwwm semhv ihfbj udwlf okvli rvjxw atapj njovw kynmr dyujd ytddu hkeww gbulq ygqum hhcen uefss smbgv qveod iijma wthod xysnx chsfy mxvsw guxuw vqfcv eirkf jimbk srhvk gfvhn pymiy zotht g"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testRotorVIII() throws {
        var enigma = try! Enigma(rotors: [.VIII, .VIII, .VIII], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "rlllh phalp jpsqt iircp nnngv oupav ayhrh rlolq qcikn pghgk guixd txkom jfkjd ckfxs ujvqi safla lzutg dgnng uwkcd glgpl ireuy yolft lbwhj ukwzq jljlw blvhd hlybn ldaxa effhl aisko kgkue ggynl gzmuk edzja ghjbo wxkxn xpaaz baubj cpdew p"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testMixedRotors() throws {
        var enigma = try! Enigma(rotors: [.I, .II, .III], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "ainfv ictqk nxsny gmrev zpetf eolfs xyhey bogzt plyha qqnzs wkbjm kfeeo qcozx jsfmu egvqu tbmtk khplt eyqck rsitb btovq plqfp ycqzt wfgdg bpepd muunu hvssb wuhmm ykcde umiic wiwec psupb gjsar suhrd asjee xpwyy fmwiz hyzhh idffi fpbod e"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
        
    }
}
