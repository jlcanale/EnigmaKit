//
//  ReflectorTests.swift
//  
//
//  Created by Joe Canale on 4/25/22.
//

import XCTest
import EnigmaKit

class ReflectorTests: XCTestCase {
    func testReflectorB() throws {
        var enigma = try! Enigma(rotors: [.I, .II, .III], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "ainfv ictqk nxsny gmrev zpetf eolfs xyhey bogzt plyha qqnzs wkbjm kfeeo qcozx jsfmu egvqu tbmtk khplt eyqck rsitb btovq plqfp ycqzt wfgdg bpepd muunu hvssb wuhmm ykcde umiic wiwec psupb gjsar suhrd asjee xpwyy fmwiz hyzhh idffi fpbod e"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
        
    }
    
    func testReflectorC() throws {
        var enigma = try! Enigma(rotors: [.I, .II, .III], reflector: .C, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "cqppn frnmm ogugt uzjkp nofcy wchqo upcoz qxjrb spelk zugvi zepee djftg aibkv xohgp toaya cvlsc xvnvo psmgp gjmzw mugzp ghddf pgnbl xrium yzfmi urkjy ybcww jkwvv nszrv cfwvn oystx oxfec prnso lfcux dnfog rmanb rwppe elgdr egykj mvcvg n"

        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
        
    }
}
