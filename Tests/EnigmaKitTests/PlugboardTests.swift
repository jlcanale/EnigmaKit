//
//  PlugboardTests.swift
//  
//
//  Created by Joe Canale on 4/25/22.
//

import XCTest
@testable import EnigmaKit

class PlugboardTests: XCTestCase {
    func testEmptyPlugboard() throws {
        var enigma = try! Enigma(rotors: [.I, .II, .III], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "ainfv ictqk nxsny gmrev zpetf eolfs xyhey bogzt plyha qqnzs wkbjm kfeeo qcozx jsfmu egvqu tbmtk khplt eyqck rsitb btovq plqfp ycqzt wfgdg bpepd muunu hvssb wuhmm ykcde umiic wiwec psupb gjsar suhrd asjee xpwyy fmwiz hyzhh idffi fpbod e"
        
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testSmallPlugboard() throws {
        var enigma = try! Enigma(rotors: [.I, .II, .III], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [Plug(("A", "Z"))])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "zinfv igtqk nxsny gmrev apeth xolfs xyhey bogat plyhz qqnas wkbjm mfeeo quoax jsfmu sgvqu tbmtk khpyt eyqck rsitb btovj plqfp ycqzt wfgdg bpepd muinu hvssb wbhmm ykcde utiic wiwec psupb gjszr suhrd zsjee xpwyy fmwia hyahh idffi fpgod e"
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testLargePlugboard() throws {
        var enigma = try! Enigma(rotors: [.I, .II, .III], reflector: .B, rotorPositions: [0,0,0], ringSettings: [0,0,0], plugboardConnections: [Plug(("A", "Z")), Plug(("B", "Q")), Plug(("C", "T")), Plug(("E", "R")), Plug(("W", "O")), Plug(("I", "L")), Plug(("K", "G")), Plug(("D", "U")), Plug(("M", "Y")), Plug(("N", "F"))])

        let input = "boot klar x bei j schnoor j etwa zwo siben x nov x sechs nul cbm x proviant bis zwo nul x dez x benoetige glaeser y noch vier klar x stehe marqu bruno bruno zwo funf x lage wie j schaefer j x nnn www funf y eins funf mb steigend y gute sicht vvv j rasch"
        let expectedOutput = "tkmqn ikmbn vesfd ktvbp abozh xouno hjjrz vwkai eimpy azxvs ohujq yfjvo bdokw wdnnt sklip sjqnk vcmmn rwnut vhlcw kbjhj uibgf mgwzo smjse xvssd ualsp oxyxq wqbkx fituy dcdsd tlolz libgt lvcwr gehld sxaur ucfqb fxewv msaxi lrnnl nykwh r"
        
        XCTAssertEqual(enigma.encrypt(input), expectedOutput.replacingOccurrences(of: " ", with: "").uppercased())
    }
    
    func testPlugboardDecodeSuccess() throws {
        let input = "AB CD EF GH"
        let expectedOutput: [Plug] = [Plug(("A", "B")), Plug(("C", "D")), Plug(("E", "F")), Plug(("G", "H"))]
        
        XCTAssert(try! Plugboard.decode(input: input) == expectedOutput)
    }
    
    func testPlugboardDecodeDuplicatePlug() throws {
        let input = "AB CD AF GH"
        
        XCTAssertThrowsError(try Plugboard.decode(input: input))
    }
    
    func testPlugboardDecodeInvalidCharacter() throws {
        let input = "AB CD EF G8"
        
        XCTAssertThrowsError(try Plugboard.decode(input: input))
    }
    
    func testPlugboardDecodeInvalidPairLength() throws {
        let input = "ABL CD EF GH"
        
        XCTAssertThrowsError(try Plugboard.decode(input: input))
    }
    
}
