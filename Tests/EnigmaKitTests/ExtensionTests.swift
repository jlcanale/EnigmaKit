//
//  ExtensionTests.swift
//  
//
//  Created by Joe Canale on 4/26/22.
//

import XCTest
@testable import EnigmaKit

class ExtensionTests: XCTestCase {
    func testArrayFillFromEmpty() {
        var input = [Int]()
        input.fill(to: 3, with: 0)
        XCTAssertEqual(input, [0,0,0])
    }
    
    func testArrayFillFromNotEmpty() {
        var input = [1]
        input.fill(to: 3, with: 0)
        XCTAssertEqual(input, [1,0,0])
    }
    
    func testArrayFillFullArray() {
        var input = [1,2,3]
        input.fill(to: 3, with: 0)
        XCTAssertEqual(input, [1,2,3])
    }
    
    func testArrayFillOverfullArray() {
        var input = [1,2,3,4,5,6]
        input.fill(to: 3, with: 0)
        XCTAssertEqual(input, [1,2,3,4,5,6])
    }
}
